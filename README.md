# 基于miniRV的SoC设计

基于miniRV（兼容RISC-V）的SoC设计，为单周期cpu和流水线cpu部分，使用verilog编写。项目设计了一个简单的计算器，可以用于下板验证（芯片型号为XC7A100TFGG484-1）。其中，流水线cpu的目录结构如下所示。
``` bash
|-- pipeline
|   |-- Bridge.v # 总线
|   |-- DEVICES_CTRL # 设备控制模块
|   |   |-- btn_ctrl.v
|   |   |-- dig_ctrl.v
|   |   |-- dig_display.v
|   |   |-- led_ctrl.v
|   |   `-- sw_ctrl.v
|   |-- EXE # 执行模块
|   |   |-- ALU.v
|   |   `-- execute.v
|   |-- HAZARD_DETECT # 冒险检测模块
|   |   |-- control_hazard_detect.v
|   |   `-- data_hazard_detect.v
|   |-- ID # 译码模块
|   |   |-- RF.v
|   |   |-- SEXT.v
|   |   |-- control.v
|   |   `-- idecode.v
|   |-- IF # 取址模块
|   |   |-- NPC.v
|   |   |-- PC.v
|   |   `-- ifetch.v
|   |-- PIPELINE_REGS # 流水线寄存器模块
|   |   |-- EXE_MEM.v
|   |   |-- ID_EXE.v
|   |   |-- IF_ID.v
|   |   `-- MEM_WB.v
|   |-- WB # 写回模块
|   |   `-- writeBack.v
|   |-- calculator.asm # 计算器汇编代码
|   |-- calculator.coe # 计算器机器码
|   |-- count.v
|   |-- defines.vh
|   |-- miniRV_SoC.v # SoC
|   `-- myCPU.v # cpu

```
本项目需要特别说明的功能在于：
1. 将CPU分为IF（取址）、ID（译码）、EXE（执行）、MEM（访存）、WB（写回）五个阶段，并为段与段之间设计流水线寄存器，实现流水线执行指令的功能。
2. 利用静态分支预测解决控制冒险问题，具体预测为分支总是不跳转。
3. 通过时钟上升沿与下降沿的时间差避免了写回阶段可能出现的数据冒险，而其余冒险通过流水线停顿解决。
4. 实现了SoC与七段数码管、LED灯、拨码开关、按键的外设I/O接口电路模块，方便下板验证。