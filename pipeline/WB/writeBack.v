module writeBack(
	input [1:0] rf_wsel,
	input [31:0] pc4,
	input [31:0] ext,
	input [31:0] ALU_C,
	input [31:0] rdo,
	output reg [31:0] wD
);


/*
rf_wsel = 0 for wD = PC + 4
rf_wsel = 1 for wD = ext
rf_wsel = 2 for wD = ALU_C
rf_wsel = 3 for wD = rdo
*/
always @ (*) begin
	case(rf_wsel)
		2'b00: wD = pc4;
		2'b01: wD = ext;
		2'b10: wD = ALU_C;
		2'b11: wD = rdo;
		default: wD = 4'h0000;
	endcase
end

endmodule