MAIN:
	lui  s0, 0xFFFFF # save base address
	lw s1, 0x070(s0) # load instruction
	srli s2, s1, 21 # set s2 as op
	srli s3, s1, 8
	andi s3, s3, 0xff # set s3 as A
	andi s4, s1, 0xff # set s4 as B
	addi s5, x0, 0
	beq s2, s5, AND
	addi s5, x0, 1
	beq s2, s5, OR
	addi s5, x0, 2
	beq s2, s5, XOR
	addi s5, x0, 3
	beq s2, s5, SLL
	addi s5, x0, 4
	beq s2, s5, SRA
	addi s5, x0, 5
	beq s2, s5, CPL
	addi s5, x0, 6
	beq s2, s5, DIV
	
AND:
	and s6, s3, s4
	jal FINAL
OR:
	or s6, s3, s4
	jal FINAL
XOR:
	xor s6, s3, s4
	jal FINAL
SLL:
	sll s6, s3, s4
	jal FINAL
SRA: 
	andi s7, s3, 0x7f
	srl s6, s7, s4
	beq s3, s7, POS # if negetive
	addi s6, s6, 0x80
	POS:
	jal FINAL
CPL:
	add s6, s4, x0
	beq s3, x0, TRUE # if convert to complement
	andi s7, s6, 0x7f
	beq s6, s7, TRUE # if negetive
	xori s6, s6, 0xff
	addi s6, s6, 0x81 # add 1000_0000 for negetive sign
	andi s6, s6, 0xff # only display 8 bit
	TRUE:
	jal FINAL
DIV:
	andi s7, s3, 0x7f # convert to unsigned
	andi s8, s4, 0x7f # convert to unsigned
	addi s9, x0, 0
	addi s10, x0, 0
	beq s3, s7, APOS
	addi s9, x0, 1 # sign of A
	APOS:
	beq s4, s8, BPOS
	addi s10, x0, 1 # sign of B
	BPOS:
	addi s6, x0, 0
	beq s8, x0, ERROR # B = 0
	beq s9, s10, LOOP
	addi s6, x0, 0x80 # sign of  C
	LOOP:
	blt s7, s8, FINAL # if A < B
	sub s7, s7, s8
	addi s6, s6, 1
	jal LOOP
	ERROR:
	lui s6,  0xFFFFF
	addi s7, x0, 0xF
	add s6, s6, s7
	slli, s7, s7, 4
	add s6, s6, s7
	slli, s7, s7, 4
	add s6, s6, s7 # create 0xFFFF_FFFF
	jal FINAL
FINAL:
	sw   s6, 0x00(s0)
	jal MAIN
