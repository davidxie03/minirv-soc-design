module PC (
	input wire rst,
	input wire clk,
	input wire [1:0] susp,
	input wire [31:0] din, // The length of addr is 14, but using unified input length facilitate the work.
	output reg [31:0] pc
);

reg flag;
wire cnt_end;

count count (
	.clk(clk),
	.rst(rst),
	.susp(susp),
	.cycle(susp[1] + 2),
	.cnt_end(cnt_end)
);

always @ (posedge rst or posedge clk) begin
	if (rst) begin
		pc <= 32'h0000;
		flag <= 1'b0;
	end
	else if (flag == 1'b0) flag <= 1'b1; // to meet the requirement of trace
	else if (susp & !cnt_end) pc <= pc;
	else pc <= din;
end

endmodule