module IF_ID (
	input wire clk,
	input wire rst,
	input wire flush,
	input wire [1:0] susp,
	input wire [31:0] inst_pre,
	input wire [31:0] pc4_pre,
	output reg [31:0] inst_next,
	output reg [31:0] pc4_next
);

wire cnt_end;

count count (
	.clk(clk),
	.rst(rst),
	.susp(susp),
	.cycle(susp[1] + 2),
	.cnt_end(cnt_end)
);

always @ (posedge clk or posedge rst) begin
	if (rst) begin
		inst_next <= 32'h0000;
		pc4_next <= 32'h0000;
	end
	else if (susp & !cnt_end) begin
		inst_next <= inst_next;
		pc4_next <= pc4_next;
	end
	else if (flush) begin
		inst_next <= 32'h0000;
		pc4_next <= 32'h0000;
	end
	else begin
		inst_next <= inst_pre;
		pc4_next <= pc4_pre;
	end
end

endmodule