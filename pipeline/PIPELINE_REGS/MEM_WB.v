module MEM_WB (
	input wire clk,
	input wire rst,
	input wire rf_we_pre,
	input wire [1:0] rf_wsel_pre,
	input wire [31:0] pc4_pre,
	input wire [31:0] rdo_pre,
	input wire [31:0] C_pre,
	input wire [31:0] ext_pre,
	input wire [4:0] wR_pre,
	output reg rf_we_next,
	output reg [1:0] rf_wsel_next,
	output reg [31:0] pc4_next,
	output reg [31:0] rdo_next,
	output reg [31:0] C_next,
	output reg [31:0] ext_next,
	output reg [4:0] wR_next
);

always @ (posedge clk  or posedge rst) begin
	if (rst) begin
		rf_we_next <= 0;
		rf_wsel_next <= 0;
		pc4_next <= 0;
		rdo_next <= 0;
		C_next <= 0;
		ext_next <= 0;
		wR_next <= 0;
	end
	else begin
		rf_we_next <= rf_we_pre;
		rf_wsel_next <= rf_wsel_pre;
		pc4_next <= pc4_pre;
		rdo_next <= rdo_pre;
		C_next <= C_pre;
		ext_next <= ext_pre;
		wR_next <= wR_pre;
	end
end

endmodule