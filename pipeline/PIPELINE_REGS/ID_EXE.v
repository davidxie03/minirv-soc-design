module ID_EXE (
	input wire clk,
	input wire rst,
	input wire flush,
	input wire [1:0] susp,
	input wire [1:0] npc_op_pre,
	input wire rf_we_pre,
	input wire [1:0] rf_wsel_pre,
	input wire ram_we_pre,
	input wire [3:0]alu_op_pre,
	input wire alub_sel_pre,
	input wire [31:0] pc4_pre,
	input wire [31:0] ext_pre,
	input wire [31:0] rD1_pre,
	input wire [31:0] rD2_pre,
	input wire [4:0] wR_pre,
	output reg [1:0] npc_op_next,
	output reg rf_we_next,
	output reg [1:0] rf_wsel_next,
	output reg ram_we_next,
	output reg [3:0]alu_op_next,
	output reg alub_sel_next,
	output reg [31:0] pc4_next,
	output reg [31:0] ext_next,
	output reg [31:0] rD1_next,
	output reg [31:0] rD2_next,
	output reg [4:0] wR_next
);

wire cnt_end;

count count (
	.clk(clk),
	.rst(rst),
	.susp(susp),
	.cycle(susp[1] + 2),
	.cnt_end(cnt_end)
);

always @ (posedge clk or posedge rst) begin
	if (rst) begin
		npc_op_next <= 0;
		rf_we_next <= 0;
		rf_wsel_next <= 0;
		ram_we_next <= 0;
		alu_op_next <= 0;
		alub_sel_next <= 0;
		pc4_next <= 0;
		ext_next <= 0;
		rD1_next <= 0;
		rD2_next <= 0;
		wR_next <= 0;
	end
	else if (susp & !cnt_end) begin
		npc_op_next <= npc_op_next;
		rf_we_next <= rf_we_next;
		rf_wsel_next <= rf_wsel_next;
		ram_we_next <= ram_we_next;
		alu_op_next <= alu_op_next;
		alub_sel_next <= alub_sel_next;
		pc4_next <= pc4_next;
		ext_next <= ext_next;
		rD1_next <= rD1_next;
		rD2_next <= rD2_next;
		wR_next <= wR_next;
	end
	else if (flush) begin
		npc_op_next <= 0;
		rf_we_next <= 0;
		rf_wsel_next <= 0;
		ram_we_next <= 0;
		alu_op_next <= 0;
		alub_sel_next <= 0;
		pc4_next <= 0;
		ext_next <= 0;
		rD1_next <= 0;
		rD2_next <= 0;
		wR_next <= 0;
	end
	else begin
		npc_op_next <= npc_op_pre;
		rf_we_next <= rf_we_pre;
		rf_wsel_next <= rf_wsel_pre;
		ram_we_next <= ram_we_pre;
		alu_op_next <= alu_op_pre;
		alub_sel_next <= alub_sel_pre;
		pc4_next <= pc4_pre;
		ext_next <= ext_pre;
		rD1_next <= rD1_pre;
		rD2_next <= rD2_pre;
		wR_next <= wR_pre;
	end
end	

endmodule