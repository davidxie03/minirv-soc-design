module idecode (
	input wire rst,
	input wire clk,
	input wire we,
	input wire [2:0] sext_op,
	input wire [4:0] wR,
	input wire [24:0] inst, // inst[31:7]
	input wire [31:0] wD,
	output wire [31:0] ext,
	output wire [31:0] rD1,
	output wire [31:0] rD2
);

RF RF (
	.clk(clk),
	.rst(rst),
	.we(we),
	.rR1(inst[12:8]), // inst[19:15]
	.rR2(inst[17:13]), // inst[24:20]
	.wR(wR), // inst[11:7]
	.wD(wD),
	.rD1(rD1),
	.rD2(rD2)
);

SEXT SEXT (
	.op(sext_op),
	.din(inst), // // inst[31:7]
	.ext(ext)
);

endmodule