module SEXT (
	input wire [2:0]op,
	input wire [24:0]din, // for IROM.inst[31:7]
	output reg [31:0] ext
);

/*
op = 0 for IROM.inst[31:20]
op = 1 for IROM.inst[31:12]
op = 2 for IROM.inst[25:20]
op = 3 for IROM.inst[31:25|11:7]
op = 4 for IROM.inst[31|7|30:25|11:8]
op = 5 for IROM.inst[31|19:12|20|30:21]
*/
always @ (*) begin
	case(op)
		3'b000: ext = {{20{din[24]}}, din[24:13]};
		3'b001: ext = {din[24:5], 12'b0};
		3'b010: ext = {{26{din[18]}}, din[18:13]};
		3'b011: ext = {{20{din[24]}}, din[24:18], din[4:0]};
		3'b100: ext = {{19{din[24]}}, din[24], din[0], din[23:18], din[4:1], 1'b0};
		3'b101: ext = {{11{din[24]}}, din[24], din[12:5], din[13], din[23:14], 1'b0};
		default: ext = 32'h0000;
	endcase
end

endmodule