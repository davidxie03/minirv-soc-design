module RF (
	input wire clk,
	input wire rst,
	input wire we,
	input wire [4:0] rR1,
	input wire [4:0] rR2,
	input wire [4:0] wR,
	input wire [31:0] wD,
	output reg [31:0] rD1,
	output reg [31:0] rD2
);

reg[31:0] regFiles[31:0];

/*
read
*/
always @ (*) begin
	rD1 = regFiles[rR1];
	rD2 = regFiles[rR2];
end

/*
write
*/
integer i;
always @ (posedge rst or negedge clk) begin
	if (rst) for (i = 0; i < 32; i = i + 1) regFiles[i] <= 32'h0000;
	else if (we == 1 & wR != 0) regFiles[wR] <= wD;
end

endmodule