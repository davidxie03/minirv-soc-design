module control_hazard_detect (
	input branch,
	output flush
);

assign flush = branch;

endmodule