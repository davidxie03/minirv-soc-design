module data_hazard_detect (
	input [1:0] ID_rRe,
	input [4:0] ID_rR1,
	input [4:0] ID_rR2,
	input [4:0] EXE_wR,
	input EXE_we,
	input [4:0] MEM_wR,
	input MEM_we,
	input [4:0] WB_wR,
	input WB_we,
	output reg [4:0] suspend // one extra bit for counting
);

reg [4:0] suspend_temp_rR1;
reg [4:0] suspend_temp_rR2;

// conflict of rR1
always @ (*) begin
	if (ID_rRe[1] == 1'b1 & ID_rR1 != 0) begin
		if (EXE_we == 1'b1 & EXE_wR == ID_rR1) suspend_temp_rR1 = 5'b1_1110;
		else if (MEM_we == 1'b1 & MEM_wR == ID_rR1) suspend_temp_rR1 = 5'b0_1111;
		else suspend_temp_rR1 = 5'b00000;
	end
	else suspend_temp_rR1 = 5'b00000;
end

// conflict of rR2
always @ (*) begin
	if (ID_rRe[0] == 1'b1 & ID_rR2 != 0) begin
		if (EXE_we == 1'b1 & EXE_wR == ID_rR2) suspend_temp_rR2 = 5'b1_1110;
		else if (MEM_we == 1'b1 & MEM_wR == ID_rR2) suspend_temp_rR2 = 4'b0_1111;
		else suspend_temp_rR2 = 5'b00000;
	end
	else suspend_temp_rR2 = 5'b00000;
end

always @ (*) begin
	if (suspend_temp_rR1 == 5'b1_1110 | suspend_temp_rR2 == 5'b1_1110) suspend = 5'b1_1110;
	else suspend = suspend_temp_rR1 | suspend_temp_rR2;
end

endmodule