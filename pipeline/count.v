module count (
	input clk,
	input rst,
	input [1:0] susp,
	input [15:0] cycle, // max num = 65535
	output cnt_end
);

reg [31:0] cnt;
reg cnt_inc;
	
assign cnt_end = cnt_inc && (cnt == cycle-1);

always @(posedge clk or posedge rst) begin
    if (rst) cnt_inc <= 0;
    else cnt_inc <= 1;
end

always @ (posedge clk or posedge rst) begin
    if (rst) cnt <= 0;
	else if (susp == 0) cnt <= 0;
	else if (cnt_end) cnt <= 0;
    else if (cnt_inc)  cnt <= cnt + 1;
end

endmodule