module dig_ctrl (
	input clk,
	input rst,
	input [31:0] data,
	input data_en,
	output reg [7:0] led_en,
	output wire DN_A,
    output wire DN_B,
    output wire DN_C,
    output wire DN_D,
    output wire DN_E,
    output wire DN_F,
    output wire DN_G,
    output wire DN_DP
);

reg [3:0] num;
reg [31:0] data_valid;
wire [6:0] transfer;

wire cnt_end;

count count (
	.clk(clk),
	.rst(rst),
	.susp(2'b11),
	.cycle(50000),
	.cnt_end(cnt_end)
);

always @ (posedge clk or posedge rst) begin
	if (rst) led_en <= 8'b1111_1111;
	else if (led_en == 8'b1111_1111) led_en <= 8'b1111_1110;
	else if (cnt_end) led_en <= {led_en[6:0], led_en[7]};
end

always @ (posedge clk or posedge rst) begin
	if (rst) data_valid <= 32'hffffffff;
	else if (data_en) data_valid <= data;
end

always @ (*) begin
	case (led_en) 
		8'b1111_1110: num = data_valid[3:0];
		8'b1111_1101: num = data_valid[7:4];
		8'b1111_1011: num = data_valid[11:8];
		8'b1111_0111: num = data_valid[15:12];
		8'b1110_1111: num = data_valid[19:16];
		8'b1101_1111: num = data_valid[23:20];
		8'b1011_1111: num = data_valid[27:24];
		8'b0111_1111: num = data_valid[31:28];
		default: num = 4'b1111;
	endcase
end

dig_display dig_display (
	.num(num),
	.transfer(transfer)
);

assign DN_A = transfer[6];
assign DN_B = transfer[5];
assign DN_C = transfer[4];
assign DN_D = transfer[3];
assign DN_E = transfer[2];
assign DN_F = transfer[1];
assign DN_G = transfer[0];
assign DN_DP = 1'b1;

endmodule