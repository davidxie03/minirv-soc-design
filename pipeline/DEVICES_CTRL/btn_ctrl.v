module btn_ctrl (
	input clk,
	input rst,
	input [4:0] button,
	output reg [4:0] rdata
);

always @ (posedge clk or posedge rst) begin
	if (rst) rdata <= 5'b00000;
	else rdata <= button;
end

endmodule