module idecode (
	input wire rst,
	input wire clk,
	input wire we,
	input wire [1:0] rf_wsel,
	input wire [2:0] sext_op,
	input wire [31:0] rdo,
	input wire [31:0] ALU_C,
	input wire [31:0] inst,
	input wire [31:0] pc4,
	output wire [31:0] ext,
	output wire [31:0] rD1,
	output wire [31:0] rD2,
	output reg [31:0] wD
);

RF RF (
	.clk(clk),
	.rst(rst),
	.we(we),
	.rR1(inst[19:15]),
	.rR2(inst[24:20]),
	.wR(inst[11:7]),
	.wD(wD),
	.rD1(rD1),
	.rD2(rD2)
);

SEXT SEXT (
	.op(sext_op),
	.din(inst[31:7]),
	.ext(ext)
);

/*
rf_wsel = 0 for wD = PC + 4
rf_wsel = 1 for wD = ext
rf_wsel = 2 for wD = ALU_C
rf_wsel = 3 for wD = rdo
*/
always @ (*) begin
	case(rf_wsel)
		2'b00: wD = pc4;
		2'b01: wD = ext;
		2'b10: wD = ALU_C;
		2'b11: wD = rdo;
		default: wD = 4'h0000;
	endcase
end

endmodule