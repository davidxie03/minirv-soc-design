module led_ctrl (
	input clk,
	input rst,
	input [7:0] en,
	input [31:0] data,
	output reg [23:0] led
);

always @ (posedge clk or posedge rst) begin
	if (rst) led <= 24'h000;
	else if (en) led <= {data[23:0]};
end

endmodule