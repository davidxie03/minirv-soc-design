module sw_ctrl (
	input clk,
	input rst,
	input [23:0] switches,
	output reg [31:0] rdata
);

always @ (posedge clk or posedge rst) begin
	if (rst) rdata <= 32'h0000;
	else rdata <= {8'h00, switches[23:0]};
end

endmodule