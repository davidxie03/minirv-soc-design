module dig_display (
	input [3:0] num,
	output reg [6:0] transfer
);

always @ (*) begin
	//低电位显示
	case(num)
	4'h0: transfer = 7'b0000001;
    4'h1: transfer = 7'b1001111;
    4'h2: transfer = 7'b0010010;
    4'h3: transfer = 7'b0000110;
    4'h4: transfer = 7'b1001100;
    4'h5: transfer = 7'b0100100;
    4'h6: transfer = 7'b0100000;
    4'h7: transfer = 7'b0001111;
    4'h8: transfer = 7'b0000000;
    4'h9: transfer = 7'b0001100;
	4'ha: transfer = 7'b0001000;
	4'hb: transfer = 7'b1100000;
	4'hc: transfer = 7'b1110010;
	4'hd: transfer = 7'b1000010;
	4'he: transfer = 7'b0110000;
	4'hf: transfer = 7'b0111000;
    default: transfer = 7'b1111110;
    endcase
end

endmodule
