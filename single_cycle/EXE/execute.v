module execute (
	input wire alub_sel,
	input wire [3:0] alu_op,
	input wire [31:0] rD1,
	input wire [31:0] rD2,
	input wire [31:0] ext,
	output wire f,
	output wire [31:0] C
);

reg [31:0] B;

/*
choose the input of B
*/
always @ (*) begin
	case (alub_sel)
		1'b0: B = rD2;
		1'b1: B = ext;
	endcase
end

ALU ALU (
	.op(alu_op),
	.A(rD1),
	.B(B),
	.C(C),
	.f(f)
);

endmodule