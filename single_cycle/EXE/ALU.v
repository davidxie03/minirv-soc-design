module ALU (
	input wire [3:0] op,
	input wire [31:0] A,
	input wire [31:0] B,
	output reg [31:0] C,
	output reg f
);

parameter ADD = 4'd0;
parameter SUB = 4'd1;
parameter AND = 4'd2;
parameter OR  = 4'd3;
parameter XOR = 4'd4;
parameter SLL = 4'd5;
parameter SRL = 4'd6;
parameter SRA = 4'd7;
parameter BEQ = 4'd8;
parameter BNE = 4'd9;
parameter BLT = 4'd10;
parameter BGE = 4'd11;

wire signed [31:0] a;
wire signed [31:0] b;

assign a = A;
assign b = B;

always @ (*) begin
	case (op)
		ADD: begin
				C = a + b;
				f = 0;
			 end
		SUB: begin
				C = a - b;
				f = 0;
			 end
		AND: begin
				C = a & b;
				f = 0;
			 end
		OR : begin
				C = a | b;
				f = 0;
			 end
		XOR: begin
				C = a ^ b;
				f = 0;
			 end
		SLL: begin
				C = a << b[4:0];
				f = 0;
			 end
		SRL: begin
				C = a >> b[4:0];
				f = 0;
			 end
		SRA: begin
				C = a >>> b[4:0];
				f = 0;
			 end
		BEQ: begin
				C = a - b;
				f = (a == b) ? 1 : 0;
			 end
		BNE: begin
				C = a - b;
				f = (a != b) ? 1 : 0;
			 end
		BLT: begin
				C = a - b;
				f = (a < b) ? 1 : 0;
			 end
		BGE: begin
				C = a - b;
				f = (a >= b) ? 1 : 0;
			 end
		default: begin
					C = 0;
					f = 0;
				 end
	endcase
end

endmodule