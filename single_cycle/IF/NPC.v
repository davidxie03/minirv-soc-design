module NPC (
	input wire br,
	input wire [1:0] op,
	input wire [31:0] PC,
	input wire [31:0] offset,
	input wire [31:0] radr,
	output reg [31:0] npc,
	output wire [31:0] pc4
);

/*
op = 0 for npc = PC + 4
op = 1 for npc = br ? PC+offset : PC+4
op = 2 for npc = PC + offset
op = 3 for npc = radr + offset
*/

always @ (*) begin
	if (op == 0) npc = PC + 4;
	else if (op == 1) begin 
		if (br == 0) npc = PC + 4;
		else if (br == 1) npc = PC + offset;
		else npc = PC;
	end
	else if (op == 2) npc = PC + offset;
	else if (op == 3) npc = radr + offset;
	else npc = PC + 4;
end

assign pc4 = PC + 4;

endmodule