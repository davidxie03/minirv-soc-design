module ifetch (
	input wire rst,
	input wire clk,
	input wire br,
	input wire [1:0] op,
	input wire [31:0] offset,
	input wire [31:0] radr,
	output wire [31:0] pc4,
	output wire [31:0] pc
);

wire [31:0] npc;

NPC NPC (
	.br(br),
	.op(op),
	.PC(pc),
	.offset(offset),
	.radr(radr),
	.npc(npc),
	.pc4(pc4)
);

PC PC (
	.rst(rst),
	.clk(clk),
	.din(npc),
	.pc(pc)
);

endmodule