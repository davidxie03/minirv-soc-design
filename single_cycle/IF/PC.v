module PC (
	input wire rst,
	input wire clk,
	input wire [31:0] din, // The length of addr is 14, but using unified input length facilitate the work.
	output reg [31:0] pc
);

reg flag;

always @ (posedge rst or posedge clk) begin
	if (rst) begin
		pc <= 32'h0000;
		flag <= 1'b0;
	end
	else if (flag == 1'b0) flag <= 1'b1;
	else pc <= din;
end
endmodule